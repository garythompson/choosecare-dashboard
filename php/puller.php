<?php

$ch = curl_init("https://api.bitbucket.org/2.0/repositories/g2g3/?pagelen=100");
$options = array(
                CURLOPT_SSL_VERIFYPEER => false,
                CURLOPT_HTTPAUTH => CURLAUTH_BASIC,
                CURLOPT_USERPWD => 'g2g3-dashboard' . ':' . ';7O5kjrrqaHA;Saj',
                CURLOPT_HTTPHEADER => array('Accept: application/json'),
                CURLOPT_RETURNTRANSFER => 1
           );
curl_setopt_array($ch, $options);
$out = curl_exec($ch);
curl_close($ch);

$json = json_decode($out,true);

$repos = array();
foreach($json['values'] as $key=>$value)
{
  foreach($value as $subkey=>$subval)
  {
    if($subkey=='name' && substr($subval,0,5)=='congo')
    {
      $repos[]=$subval;
    }
  }
}

$total = 0;
$html = array();

foreach($repos as $repo)
{
  $ch = curl_init("https://api.bitbucket.org/2.0/repositories/g2g3/$repo/pullrequests");
  curl_setopt_array($ch, $options);
  $out = curl_exec($ch);
  $json = json_decode($out);
  $size = $json->size;
    
  if($size>0)
  {
    for($i=0;$i<$size;$i++)
    {
      $html[] = $json->values[$i]->links->html->href;
    }
    $total=$total+$size;
  }
  
  curl_close($ch);
}

$html = implode("\n", $html);

if($total>=1)
{
  $ch = curl_init();
  $username = curl_escape($ch, "PULL NAG");
  $text = curl_escape($ch, "There are currently $total pull requests open:\n$html");
  $query = "token=xoxp-6587603088-10297956548-14727374371-8883a2e0d3&channel=C1HDZ1EGL&username={$username}&text={$text}";
  $url = "https://slack.com/api/chat.postMessage?$query";
  curl_setopt($ch, CURLOPT_URL, $url);
  curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
  curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
  curl_exec($ch);
  curl_close($ch);
}
