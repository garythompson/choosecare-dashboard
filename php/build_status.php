<?php

$ch = curl_init("http://teamcity.g2g3.net/guestAuth/app/rest/buildTypes/id:ProjectCongo_ProjectCongoStable/builds?locator=branch:(default:any)&count=1");
$options = array(
    CURLOPT_HTTPHEADER => array('Accept: application/json'),
    CURLOPT_RETURNTRANSFER => 1
  );
curl_setopt_array($ch, $options);
$out = curl_exec($ch);
curl_close($ch);

$json = json_decode($out);

$filename = 'c:\scripts\data\build_status.txt';

$fp = fopen($filename, 'r');
$last_status = fread($fp, filesize($filename));
fclose($fp);

$current_status = $json->build[0]->status;

if(($last_status != $current_status) && $current_status=="FAILURE")
{
  exec("C:\scripts\cmdmp3new\cmdmp3win.exe c:\scripts\sounds\broken-build.mp3");
}

$fp = fopen($filename, 'w');
fwrite($fp, $current_status);
fclose($fp);
