<?php

$ch = curl_init("https://g2g3projects.atlassian.net/rest/api/2/search?jql=project=CONGO AND (issuetype=Story OR issuetype=Improvement OR issuetype=Task OR issuetype=Bug) AND sprint in openSprints() AND status = Done ORDER BY rank");
$options = array(
                  CURLOPT_SSL_VERIFYPEER => false,
                  CURLOPT_HTTPHEADER => array('Accept: application/json','Authorization: Basic Z2FyeS50aG9tcHNvbjpnZ3QgODI1TQ=='),
                  CURLOPT_RETURNTRANSFER => true
                );
curl_setopt_array($ch, $options);
$out = curl_exec($ch);
curl_close($ch);

$json = json_decode($out);
$total = $json->total;

$filename = 'c:\scripts\data\jira_count.txt';
$fp = fopen($filename, 'r');
$last_count = fread($fp, filesize($filename));
fclose($fp);

if($total > $last_count)
{
  exec("C:\scripts\cmdmp3new\cmdmp3win.exe c:\scripts\sounds\yeah.mp3");
}
if($total < $last_count)
{
  exec("C:\scripts\cmdmp3new\cmdmp3win.exe c:\scripts\sounds\boo.mp3");
}

$fp = fopen($filename, 'w');
fwrite($fp, $total);
fclose($fp);
