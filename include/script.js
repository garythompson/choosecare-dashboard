function play_sound(file) {
  var audioElement = document.createElement('audio');
  audioElement.setAttribute('src', file);
  audioElement.setAttribute('autoplay', 'autoplay');
  audioElement.load();
  audioElement.play();
}